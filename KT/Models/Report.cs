﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KT.Models
{
	public class Report
	{
		public int Id { get; set; }
		public int AccountId { get; set; }
		public int LogId { get; set; }
		public int TransactionId { get; set; }
		public string Reportname { get; set; }
		public DateTime Reportdate { get; set; }

		// Foreign Key
		[ForeignKey("AccountId")]
		public Account Account { get; set; }

		[ForeignKey("LogId")]
		public Log Log { get; set; }

		[ForeignKey("TransactionId")]
		public Transaction Transaction { get; set; }
	}
}
