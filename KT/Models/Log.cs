﻿namespace KT.Models
{
	public class Log
	{
		public int Id { get; set; }
		public int TransactionId { get; set; }
		public DateTime Logindate { get; set; }
		public DateTime LoginTime { get; set; }

		// Foreign Key
		public Transaction Transaction { get; set; }
	}
}
