﻿namespace KT.Models
{
	public class Customer
	{
		public int Id { get; set; }
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public string ContactAndAddress { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }

		// Foreign Key
		public List<Account> Accounts { get; set; }
		public List<Transaction> Transactions { get; set; }
	}
}
