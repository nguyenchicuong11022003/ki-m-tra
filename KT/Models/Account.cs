﻿namespace KT.Models
{
	public class Account
	{
		public int Id { get; set; }
		public int CustomerId { get; set; }
		public string Accountname { get; set; }

		// Foreign Key
		public Customer Customer { get; set; }
	}
}
