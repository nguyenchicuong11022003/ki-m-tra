﻿namespace KT.Models
{
	public class Employee
	{
		public int Id { get; set; }
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public string ContactAndAddress { get; set; }
		public string UsernamePassword { get; set; }

		// Foreign Key
		public List<Transaction> Transactions { get; set; }
	}
}
